---
author: bgamari
title: "GHC 9.0.1 is now available"
date: 2021-02-04
tags: release
---

The GHC team is very pleased to announce the availability of GHC 9.0.1.
Source and binary distributions are available at the [usual
place](https://downloads.haskell.org/ghc/9.0.1/).

In addition to numerous bug fixes, GHC 9.0.1 brings a number of new
features:

 * A first cut of the new `LinearTypes` [language extension][2], allowing use
   of linear function syntax and linear record fields.

 * A new bignum library, `ghc-bignum`, improving portability and allowing GHC
   to be more easily used with integer libraries other than GMP.

 * Improvements in code generation, resulting in considerable
   runtime performance improvements in some programs.

 * Improvements in pattern-match checking, allowing more precise
   detection of redundant cases and reduced compilation time.

 * Implementation of the "simplified subsumption" [proposal][3]
   simplifying the type system and paving the way for QuickLook
   impredicativity in GHC 9.2.

 * Implementation of the `QualifiedDo` [extension][4], allowing more
   convenient overloading of `do` syntax.

 * An experimental new IO manager implementation for Windows platforms, both
   improving performance and fixing many of the quirks with the old manager built
   on POSIX-emulation.

 * Improvements in compilation time.

And many more. See the [release notes][5] for a full accounting of the
changes in this release.

As always, feel free to report any issues you encounter via
[gitlab.haskell.org](https://gitlab.haskell.org/ghc/ghc/-/issues/new).

[1]: https://gitlab.haskell.org/ghc/ghc/-/wikis/migration/9.0#ghc-prim-07
[2]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0111-linear-types.rst
[3]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0287-simplify-subsumption.rst
[4]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0216-qualified-do.rst 
[5]: https://downloads.haskell.org/ghc/9.0.1/docs/html/users_guide/9.0.1-notes.html

