---
author: Zubin Duggal
title: "GHC 9.4.6 is now available"
date: 2023-08-07
tags: release
---

The GHC developers are happy to announce the availability of GHC 9.4.6. Binary
distributions, source distributions, and documentation are available at
[downloads.haskell.org](https://downloads.haskell.org/ghc/9.4.6).

This release is primarily a bugfix release addressing some issues
found in 9.4.6. These include:

 * Many bug fixes for the simplifier, preventing compiler panics, loops and
   incorrect code generation (#22761, #22549, #23208, #22761, #22272, #23146,
   #23012, #22547).
 * Bug fixes for the typechecker involving newtype family instances, making
   type equalities more robust and bugs having to do with defaulting representation
   polymorphic type variables (#23329, #23333, #23143, #23154, #23176).
 * Some bug fixes for code generation, particularly on the aarch64 backend,
   including adding more memory barriers for array read operations (#23541, #23749).
 * Some bug fixes for windows builds, ensuring the reliablility of IO manager shutdown
   and a bug fix for the RTS linker on windows (#23691, #22941).
 * A bug fix for the non-moving GC ensuring mutator allocations are properly
   accounted for (#23312).
 * A bug fix preventing some segfaults by ensuring that pinned allocations respect
   block size (#23400).
 * Many bug fixes for the bytecode interpreter, allowing a greater subset
   of the language to be interpreted (#22376, #22840, #22051, #21945, #23068, #22958).
 * ... and a few more. See the [release notes] for a full accounting.

As some of the fixed issues do affect correctness users are encouraged to
upgrade promptly.

We would like to thank Microsoft Azure, GitHub, IOG, the Zw3rk stake pool,
Well-Typed, Tweag I/O, Serokell, Equinix, SimSpace, Haskell Foundation, and
other anonymous contributors whose on-going financial and in-kind support has
facilitated GHC maintenance and release management over the years. Finally,
this release would not have been possible without the hundreds of open-source
contributors whose work comprise this release.

As always, do give this release a try and open a [ticket][] if you see
anything amiss.

Happy compiling,

- Zubin

[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new
[release notes]: https://downloads.haskell.org/~ghc/9.4.6/docs/users_guide/9.4.6-notes.html
