---
author: bgamari
title: "GHC 9.8.1-alpha1 is now available"
date: 2023-07-27
tags: release
---
The GHC developers are very pleased to announce the availability of the
first alpha prerelease of GHC 9.8.1. Binary distributions, source
distributions, and documentation are available at [downloads.haskell.org][].


GHC 9.8 will bring a number of new features and improvements, including:

 * Preliminary support the [`TypeAbstractions` language extension][type-binders],
   allowing types to be bound in type declarations.

 * Support for the [`ExtendedLiterals`][extended-literals] extension, providing syntax for
   non-word-sized numeric literals in the surface language

 * Improved rewrite rule matching behavior, allowing limited matching of
   higher-order patterns

 * Better support for user-defined warnings by way of the [`WARNING` pragma][warnings]

 * The introduction of the new [`GHC.TypeError.Unsatisfiable`][unsatisfiable]
   constraint, allowing more predictable user-defined type errors

 * Implementation of the [export deprecation proposal][deprecated-exports], allowing module
   exports to be marked with `DEPRECATE` pragmas

 * The addition of [build semaphore support][jsem] for parallel compilation;
   with coming support in `cabal-install` this will allow better use of
   parallelism in multi-package builds

 * More efficient representation of info table provenance information,
   reducing binary sizes by over 50% in some cases when
   `-finfo-table-map` is in use

A full accounting of changes can be found in the [release notes][].

We would like to thank GitHub, IOG, the Zw3rk stake pool,
Well-Typed, Tweag I/O, Serokell, Equinix, SimSpace, the Haskell
Foundation, and other anonymous contributors whose on-going financial
and in-kind support has facilitated GHC maintenance and release
management over the years. Finally, this release would not have been
possible without the hundreds of open-source contributors whose work
comprise this release.

As always, do give this release a try and open a [ticket][] if you see
anything amiss.


[downloads.haskell.org]: https://downloads.haskell.org/ghc/9.8.1-alpha1
[type-binders]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0425-decl-invis-binders.rst
[extended-literals]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0451-sized-literals.rst
[unsatisfiable]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0433-unsatisfiable.rst
[warnings]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0541-warning-pragmas-with-categories.rst
[deprecated-exports]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0134-deprecating-exports-proposal.rst
[jsem]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0540-jsem.rst
[release notes]: https://downloads.haskell.org/ghc/9.8.1-alpha1/docs/users_guide/9.8.1-notes.html
[ticket]: https://gitlab.haskell.org/ghc/ghc/-/issues/new

