---
author: thoughtpolice
title: "GHC Weekly News - 2014/09/30"
date: 2014-09-30
tags: ghc news
---

Hi \*,

Here's some news for y'all! Apologizes about the unavailability last week; the internet wasn't exactly a very fun place for a system administrator...

So without delay, here's the current recap of the past two weeks:

 - Lots of merged code and code reviews have gone in, and a lot of commits: in the past two weeks since the last update, `ghc.git` has seen just over 100 commits, from about a dozen different developers.

 - As part of those patches, a significant amount of them have gone towards implementing the "Burning Bridges Proposal" or BBP for the `base` library. This is a set of changes to `base` that have generalized many parts of the API, by putting `Traversable` and `Foldable` in the Prelude. This required a bit of shoveling by Herbert, but now this is all in GHC HEAD, and will be part of 7.10:
   - `Prelude` combinators, like `mapM`, have been generalized to the `Traversable` and `Foldable` classes.
   - Several other modules, like `Control.Monad` and `Data.List`, have been generalized to `Traversable` and `Foldable` where applicable.
   - `Control.Monad` combinators generalized to `Applicative` where possible.
   - Similarly, `MonadPlus` combinators like `guard` are generalized to `Alternative`.
   - `Foldable` has been extended with new methods, like `length` and `null`.

 - But also, GHC's compiler is now tab-free! That's right, after what seemed like a million years, a very large portion of the code has been detabbed, and `-fwarn-tabs` is now on by default in the GHC build.

 - There are an assortment of other changes: GHC's linker is not as loud![1], and various documentation improvements.

 - The windows build is broken *again* unfortunately, this time due to what seems to be a Cabal update, I think. Austin is once again on the case.

 - The HCAR draft for October has seen some nice improvements. If you're a developer, please amend things. If you're a user, read with eager anticipation of all the new features! [2][]

 - It turns out the new Applicative/Monad changes have unfortunately broken the `haskell98` and `haskell2010` packages, with an unclear migration strategy for the future: see #9590. For GHC 7.10, it seems the `haskell2010` packages will need to change to accomodate these standard deviations.
 If any users of the `haskell98` or `haskell2010` packages would speak up to help, that would be fantastic. The discussion will surely continue for a little bit - 7.10 is still a ways off.

In miscellaneous news:

 - ghc.haskell.org may have been temporarily unavailable during this weekend due to an emergency downtime with our provider for a security update, but the window was quite small.

 - Relatedly (but not the exact same scenario), the internet also caught fire in several other places, requiring quite a lot of careful watching and remediation after the Bash "ShellShock" bug hit last week.

And I think that sums it up quite nicely, folks!

Closed tickets for the past two weeks include (there are a lot of them!): #9650, #7068, #5190, #5880, #8374, #9603, #3303, #3470, #3509, #781, #8115, #9641, #9191, #9515, #9326, #7987, #9634, #9576, #9612, #9635, #8593, #7544, #8529, #9338, #5794, #9535, #3646, #617, #8026, #8480, #8881, #9366, #8251, #7673, #8983, #8369, #8897, #8070, #9550, #9057, #9629, #8836, #8960, #8941, #9565, #9589, #5248, #8918, #9568, #9620, #1042, #9557, #7863, #5647, #9610, #5395, #9580, #9529, #4426, #9607, #9592, #8097, #9559, #9560, #4218, #9602, #9528, #9530, #9423, #9400, #1407, #9598, #9597.

I'd like to mention that for the above tickets, a *huge* amount of them were closed by one of our newest contributors, **Thomas Miedema**, who went through the bug tracker and confirmed or closed a large majority of them. I lost track of how many. Thanks Thomas!

[1]: https://github.com/ghc/ghc/commit/9f7e3633c692dce75c27607131bd386178fb0fcf
[2]: https://ghc.haskell.org/trac/ghc/wiki/Status/Oct14
